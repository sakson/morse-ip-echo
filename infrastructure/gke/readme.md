# GKE configuration for morse-ip-echo service

## Prerequisites

* Configured Google Cloud credentials (if you don't have a k8s cluster)
* Configured `kubectl`

## Provision k8s (optional)

Use cloud shell or local gcloud cli

```bash
gcloud container clusters create "your-cluster-name"  --zone "europe-west1-c" --cluster-version "1.12.7-gke.10" --machine-type "g1-small" --disk-size "30" --num-nodes "1" --addons HorizontalPodAutoscaling,HttpLoadBalancing
```

## Deploy a service

```bash
cat << EOF > morse-ip-echo.yaml
kind: Service
apiVersion: v1
metadata:
  name: morse-ip
spec:
  selector:
    app: morse-ip
  ports:
  - protocol: TCP
    port: 80
    targetPort: 9000
  type: LoadBalancer
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: morse-ip
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: morse-ip
    spec:
      containers:
      - name: morse-ip
        image: sakson/echo
        ports:
        - containerPort: 9000
EOF

kubectl apply -f morse-ip-echo.yaml

```