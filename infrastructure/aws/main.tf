terraform {
  required_version = ">= 0.11.13"
}

provider "aws" {
  region  = "${var.region}"
  version = "~> 2.8"
}

variable "region" {
  default = "us-east-1"
}

resource "aws_lb" "nlb" {
  name               = "${var.region}-nlb"
  load_balancer_type = "network"
  internal           = false
  subnets            = ["${data.aws_subnet_ids.default.ids}"]
}

resource "aws_lb_target_group" "target-group" {
  name       = "target-group"
  depends_on = ["aws_lb.nlb"]
  port       = 9000
  protocol   = "TCP"
  vpc_id     = "${data.aws_vpc.default.id}"
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = "${aws_lb.nlb.arn}"
  port              = "9000"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.target-group.arn}"
  }
}

resource "aws_ecs_service" "morse-echo-service" {
  name            = "morse-echo-service"
  cluster         = "${aws_ecs_cluster.ecs.id}"
  task_definition = "${aws_ecs_task_definition.morse-echo-service.arn}"

  # can be scaled by changing this param or automatically by ECS
  desired_count = 2
  depends_on    = ["aws_lb_target_group.target-group", "aws_ecs_task_definition.morse-echo-service"]

  load_balancer {
    target_group_arn = "${aws_lb_target_group.target-group.arn}"
    container_name   = "morse-echo-service"
    container_port   = 9000
  }
}

resource "aws_ecs_task_definition" "morse-echo-service" {
  family                   = "morse-echo-service"
  requires_compatibilities = ["EC2"]

  container_definitions = <<DEFINITION
[
  {
    "memory": 32,
    "essential": true,
    "image": "sakson/echo:latest",
    "name": "morse-echo-service",
    "portMappings": [
      {
        "containerPort": 9000,
        "protocol": "tcp"
        }],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "ecs-logs",
        "awslogs-region": "${var.region}",
        "awslogs-stream-prefix": "morse-echo-service"
      }
    }
  }
]
DEFINITION
}

# ECS CLUSTER
resource "aws_ecs_cluster" "ecs" {
  name = "ecs-cluster"
}

resource "aws_security_group" "instance_sg" {
  ingress {
    protocol    = "tcp"
    from_port   = 32768
    to_port     = 61000
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_launch_configuration" "ecs" {
  name_prefix   = "ecs-launch-configuration"
  image_id      = "${data.aws_ami.amzn2.id}"
  instance_type = "t3.micro"

  user_data            = "${data.template_file.user_data.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.instance.name}"

  lifecycle {
    create_before_destroy = true
  }

  security_groups = [
    "${aws_security_group.instance_sg.id}",
  ]
}

resource "aws_autoscaling_group" "ecs" {
  name                 = "ecs-autoscaling"
  launch_configuration = "${aws_launch_configuration.ecs.name}"
  min_size             = 1
  max_size             = 2
  desired_capacity     = 1
  force_delete         = true
  health_check_type    = "EC2"
  vpc_zone_identifier  = ["${data.aws_subnet.subnet0.id}", "${data.aws_subnet.subnet1.id}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_cloudwatch_log_group" "ecs" {
  name = "ecs-logs"
}

resource "aws_iam_policy" "ecs" {
  name   = "ecs-instance"
  path   = "/"
  policy = "${data.aws_iam_policy_document.instance_policy.json}"
}

resource "aws_iam_role_policy_attachment" "attach1" {
  role       = "${aws_iam_role.instance.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "attach2" {
  role       = "${aws_iam_role.instance.name}"
  policy_arn = "${aws_iam_policy.ecs.arn}"
}

resource "aws_iam_role" "instance" {
  name = "instance-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "instance" {
  name = "ecs-instance-profile"
  role = "${aws_iam_role.instance.name}"
}

output "lb_dns" {
  value = "${aws_lb.nlb.dns_name}"
}

output "lb_zone_id" {
  value = "${aws_lb.nlb.zone_id}"
}
