# Infrastructure configuration for morse-ip-echo service

## Prerequisites

* Installed terraform
* Configured AWS credentials

## Provision infrastructure

By default terraform configured to provision infrastructure in us-east-1, you can change this by setting a new value for the variable `region`.
To provision infrastructure, you have to walk through a couple steps

```bash
terraform init
terraform plan -out=ecs-service
terraform apply ecs-service
```

## DNS configuration

[Link to Route53 configuration](./route53-configuration.md)