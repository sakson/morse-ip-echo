data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = "${data.aws_vpc.default.id}"
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnet" "subnet0" {
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  default_for_az    = "true"
}

data "aws_subnet" "subnet1" {
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  default_for_az    = "true"
}

data "template_file" "user_data" {
  template = "${file("./user-data.sh")}"

  vars {
    cluster_name = "${aws_ecs_cluster.ecs.name}"
  }
}

data "aws_ami" "amzn2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*"]
  }
}

data "aws_iam_policy_document" "instance_policy" {
  statement {
    sid = "CloudwatchPutMetricData"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = "InstanceLogging"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
    ]

    resources = [
      "${aws_cloudwatch_log_group.ecs.arn}",
    ]
  }

  statement {
    sid = "EcsAll"

    actions = [
      "ecs:*",
    ]

    resources = [
      "*",
    ]
  }
}
