
# Route53 geolocation records configuration

## Prerequisites

* Passed terraform deployment in your regions
* Installed AWS CLI
* Configured AWS API credentials with admin access to Route53
* Installed JSON cli parser `jq` (recommended but not required)

# Option 1 - CloudFormation and AWS CLI

Replace `ZONEIDHERE` with appropriate LoadBalancer Zone ID, `notexample.com` replace with your domain and `nlb-created-by-terraform` with appropriate LoadBalancer DNS. You can get appropriate values using the instruction from Option 2 -> Step1

Than use AWS CLI:

```bash
aws cloudformation create-stack --stack-name myteststack --template-body file://route53_cf.json --parameters \
ParameterKey=HostedZoneName,ParameterValue=notexample.com. \
ParameterKey=EULBDNS,ParameterValue=nlb-created-by-terraform.eu-west-1.elb.amazonaws.com \
ParameterKey=EULBHostedZone,ParameterValue=ZONEIDHERE \
ParameterKey=UsEastLBDNS,ParameterValue=nlb-created-by-terraform.us-east-1.elb.amazonaws.com \
ParameterKey=UsEastLBHostedZone,ParameterValue=ZONEIDHERE \
ParameterKey=DNSRecord,ParameterValue=www.notexample.com.
```

# Option 2 - json configuration

## To configure Route53 geolocation feature you have to go through several steps

## Step 1

Get LoadBalancer Hosted zone ID and DNS name:

---
**Note:**
In this example, the first LoadBalancer is chosen. You should choose yours.
---

```bash
aws elbv2 describe-load-balancers --output json | jq -r .LoadBalancers[0].CanonicalHostedZoneId
aws elbv2 describe-load-balancers --output json | jq -r .LoadBalancers[0].DNSName
```

## Step 2

Replace `LB_hosted_zone_ID` and `DNS_of_LB` with data from Step 1 in the file `route53_records.json` for all required regions.

## Step 3

Apply changes:

---
**Note:**
Replace the `ZEXAMPLEID` with your Route53 zone ID (you can find it via AWS CLI `aws route53 list-hosted-zones`)
---

```bash
aws route53 change-resource-record-sets --hosted-zone-id ZEXAMPLEID --change-batch file://route53_records.json
```

# More documentation links

Link to the documentation with available geolocations [here](https://docs.aws.amazon.com/Route53/latest/APIReference/API_GetGeoLocation.html#API_GetGeoLocation_RequestSyntax)
