package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"github.com/martinlindhe/morse"
)

const (
	connHost = ""
	connPort = "9000"
	connType = "tcp"
)

func main() {
	// Listen for incoming connections.
	l, err := net.Listen(connType, connHost+":"+connPort)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	log.Println("Listening on " + connHost + ":" + connPort)
	// Infinit loop for
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			log.Printf("error accepting connection %v", err)
			continue
		}
		log.Printf("accepted connection from %v", conn.RemoteAddr())
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	// Read the incoming connection into the buffer.
	_, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}
	// Send a response back to person contacting us.

	var strRemoteAddr string

	if addr, ok := conn.RemoteAddr().(*net.TCPAddr); ok {
		strRemoteAddr = addr.IP.String()
	}
	morseAddr := morse.EncodeITU(strRemoteAddr)
	conn.Write([]byte(morseAddr))

	// Close the connection when you're done with it.
	conn.Close()
}
