FROM golang:1.12.4 as builder
WORKDIR /build
COPY main.go go.mod go.sum ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .
FROM alpine:latest
RUN apk --no-cache add ca-certificates

WORKDIR /app
COPY --from=builder /build/app ./
EXPOSE 9000
CMD /app/app
